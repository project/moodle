<?php

/**
 * @file
 * user page callbacks for the moodle module.
 */

/**
 * Display a full page of all upcoming Moodle courses.
 * All Moodle courses which have a start date of today or later.
 */
function moodle_page() {
  $output = '';
  $intro = variable_get('moodle_page_intro', '');
  if (!empty($intro)) {
    $output = '<div id="moodle-intro"><p>' . t($intro) . '</p></div>';
  }
  $output .= _moodle_page_theme_course_list(_moodle_get_courses());
  return $output;
}

/**
 * Display a full page of a particular user's Moodle courses.
 */
function moodle_user_page($account) {
  $output = '';
  $completed = _moodle_page_theme_course_list(_moodle_get_usercourses($account->name, 'completed'));   // get completed courses
  $inprogress = _moodle_page_theme_course_list(_moodle_get_usercourses($account->name));    // get courses still in progress
  $output .= '<h3 id="moodle-course">' . t('Completed') . '</h3>';
  $output .= $completed;
  $output .= '<h3 id="moodle-course">' . t('In progress') . '</h3>';
  $output .= $inprogress;
  return $output;
}

function _moodle_page_theme_course_list($list) {
  $moodle_url = _moodle_get_url();
  if (empty($list) || !$moodle_url) {
    $content = t("No courses found.");
  }
  else {
    foreach ($list as $course) {
      $url = $moodle_url . $course['urlpath'];
      $fullname = $course['fullname'];
      $summary = $course['summary'];
      $shortname = $course['shortname'];
      $numweeks = $course['numweeks'];
      $startdate = $course['startdate'];
      $rows[] = array($shortname, _moodle_link($url, $fullname), $startdate, $numweeks);
    }
    $header = array(t('Short name'), t('Full name'), t('Start date'), t('Number of weeks or topics'));
    $content = theme_table($header, $rows);
  }
  return $content;
}

// --- END OF FILE ---
