<?php

/**
 * @file
 * Moodle Module assists with integrating a Moodle instance with a Drupal
 * instance.  This file contains the CCK node types for various kinds of
 * objects published by Moodle which we want to subscribe to.
 */

/**
 * Return a Course CCK node type as the EXPORT string which the CCK admin page
 * provides and the IMPORT function expects.
 * Note: the coder module will complain about many style issues in the export.
 */
function _moodle_course_cck_export() {

    $content['type']  = array (
      'name' => 'Course',
      'type' => 'course',
      'description' => 'An instructor led course.',
      'title_label' => 'Title',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
      'node_options' => 
      array (
        'status' => true,
        'promote' => true,
        'sticky' => false,
        'revision' => false,
        'moderate' => false,
      ),
      'save_as_draft' => 0,
      'save_as_draft_title' => '',
      'save_as_draft_body' => '',
      'language_content_type' => 0,
      'show_preview_changes' => 1,
      'forward_display' => 1,
      'notifications_node_ui' => 
      array (
        'links' => true,
        'form' => false,
        'comment' => false,
        'block' => false,
      ),
      'scheduler' => 0,
      'scheduler_touch' => 0,
      'community_tags_display' => 1,
      'i18n_node' => 1,
      'old_type' => 'course',
      'orig_type' => '',
      'module' => 'node',
      'custom' => '1',
      'modified' => '1',
      'locked' => '0',
      'og_content_type_usage' => 'omitted',
      'signup_node_default_state' => 'disabled',
      'signup_date_field' => 0,
      'rdf_schema_class' => '',
      'content_profile_use' => false,
      'comment' => '0',
      'comment_default_mode' => '4',
      'comment_default_order' => '1',
      'comment_default_per_page' => '50',
      'comment_controls' => '3',
      'comment_anonymous' => 0,
      'comment_subject_field' => '1',
      'comment_preview' => '1',
      'comment_form_location' => '0',
      'audience_check' => false,
      'file_attach_node' => 1,
      'file_attach_comment' => 1,
      'ant' => 0,
      'ant_pattern' => '',
      'ant_php' => 0,
      'feedapi' => 
      array (
        'enabled' => false,
        'refresh_on_create' => 0,
        'update_existing' => 1,
        'skip' => 0,
        'items_delete' => 0,
        'parsers' => 
        array (
          'parser_simplepie' => 
          array (
            'enabled' => false,
            'weight' => 0,
          ),
          'parser_common_syndication' => 
          array (
            'enabled' => false,
            'weight' => 0,
          ),
        ),
        'processors' => 
        array (
          'feedapi_node' => 
          array (
            'enabled' => false,
            'weight' => 0,
            'content_type' => 'story',
            'node_date' => 'feed',
            'promote' => '3',
            'x_dedupe' => 0,
          ),
          'feedapi_inherit' => 
          array (
            'enabled' => false,
            'weight' => 0,
            'inherit_og' => 1,
            'inherit_taxonomy' => 1,
          ),
        ),
      ),
    );
    $content['fields']  = array (
      0 => 
      array (
        'label' => 'Date',
        'field_name' => 'field_date',
        'type' => 'datestamp',
        'widget_type' => 'date_popup',
        'change' => 'Change basic information',
        'weight' => '-4',
        'default_value' => 'now',
        'default_value2' => 'same',
        'default_value_code' => '',
        'default_value_code2' => '',
        'input_format' => 'm/d/Y',
        'input_format_custom' => '',
        'year_range' => '-3:+3',
        'increment' => '1',
        'advanced' => 
        array (
          'label_position' => 'above',
          'text_parts' => 
          array (
            'year' => 0,
            'month' => 0,
            'day' => 0,
            'hour' => 0,
            'minute' => 0,
            'second' => 0,
          ),
        ),
        'label_position' => 'above',
        'text_parts' => 
        array (
        ),
        'description' => '',
        'group' => false,
        'required' => 1,
        'multiple' => '0',
        'repeat' => 0,
        'todate' => 'required',
        'granularity' => 
        array (
          'year' => 'year',
          'month' => 'month',
          'day' => 'day',
          'hour' => 'hour',
          'minute' => 'minute',
          'second' => 'second',
        ),
        'default_format' => 'field_date_default',
        'tz_handling' => 'date',
        'timezone_db' => 'UTC',
        'op' => 'Save field settings',
        'module' => 'date',
        'widget_module' => 'date',
        'columns' => 
        array (
          'value' => 
          array (
            'type' => 'int',
            'not null' => false,
            'sortable' => true,
          ),
          'value2' => 
          array (
            'type' => 'int',
            'not null' => false,
            'sortable' => true,
          ),
          'timezone' => 
          array (
            'type' => 'varchar',
            'length' => 50,
            'not null' => false,
            'sortable' => true,
          ),
          'offset' => 
          array (
            'type' => 'int',
            'not null' => false,
            'sortable' => true,
          ),
          'offset2' => 
          array (
            'type' => 'int',
            'not null' => false,
            'sortable' => true,
          ),
        ),
        'rdf_property_from' => 'xcal:dtstart',
        'rdf_property_to' => 'xcal:dtend',
        'display_settings' => 
        array (
          'weight' => '-4',
          'parent' => '',
          'label' => 
          array (
            'format' => 'inline',
          ),
          'teaser' => 
          array (
            'format' => 'field_date_default',
            'exclude' => 0,
          ),
          'full' => 
          array (
            'format' => 'field_date_default',
            'exclude' => 0,
          ),
          4 => 
          array (
            'format' => 'field_date_default',
            'exclude' => 0,
          ),
          2 => 
          array (
            'format' => 'field_date_default',
            'exclude' => 0,
          ),
          3 => 
          array (
            'format' => 'field_date_default',
            'exclude' => 0,
          ),
        ),
      ),
      1 => 
      array (
        'label' => 'Name of Instructor(s)',
        'field_name' => 'field_name_of_instructors',
        'type' => 'text',
        'widget_type' => 'text_textfield',
        'change' => 'Change basic information',
        'weight' => '-3',
        'rows' => 1,
        'size' => '50',
        'description' => '',
        'default_value' => 
        array (
          0 => 
          array (
            'value' => '',
            '_error_element' => 'default_value_widget][field_name_of_instructors][0][value',
          ),
        ),
        'default_value_php' => '',
        'default_value_widget' => 
        array (
          'field_name_of_instructors' => 
          array (
            0 => 
            array (
              'value' => '',
              '_error_element' => 'default_value_widget][field_name_of_instructors][0][value',
              'format' => 1,
            ),
          ),
        ),
        'group' => false,
        'required' => 0,
        'multiple' => '0',
        'text_processing' => '1',
        'max_length' => '',
        'allowed_values' => '',
        'allowed_values_php' => '',
        'op' => 'Save field settings',
        'module' => 'text',
        'widget_module' => 'text',
        'columns' => 
        array (
          'value' => 
          array (
            'type' => 'text',
            'size' => 'big',
            'not null' => false,
            'sortable' => true,
          ),
          'format' => 
          array (
            'type' => 'int',
            'unsigned' => true,
            'not null' => false,
          ),
        ),
        'rdf_property' => 'spawar:instructors',
        'display_settings' => 
        array (
          'weight' => '17',
          'parent' => 'group_details',
          'label' => 
          array (
            'format' => 'inline',
          ),
          'teaser' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          'full' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          4 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          2 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          3 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
        ),
      ),
      2 => 
      array (
        'label' => 'Course Description',
        'field_name' => 'field_body',
        'type' => 'text',
        'widget_type' => 'text_textarea',
        'change' => 'Change basic information',
        'weight' => '-2',
        'rows' => '10',
        'size' => 60,
        'description' => '',
        'default_value' => 
        array (
          0 => 
          array (
            'value' => '',
            'format' => '1',
            '_error_element' => 'default_value_widget][field_body][0][value',
          ),
        ),
        'default_value_php' => '',
        'default_value_widget' => 
        array (
          'field_body' => 
          array (
            0 => 
            array (
              'value' => '',
              'format' => '1',
              '_error_element' => 'default_value_widget][field_body][0][value',
            ),
          ),
        ),
        'group' => false,
        'required' => 1,
        'multiple' => '0',
        'text_processing' => '1',
        'max_length' => '',
        'allowed_values' => '',
        'allowed_values_php' => '',
        'op' => 'Save field settings',
        'module' => 'text',
        'widget_module' => 'text',
        'columns' => 
        array (
          'value' => 
          array (
            'type' => 'text',
            'size' => 'big',
            'not null' => false,
            'sortable' => true,
          ),
          'format' => 
          array (
            'type' => 'int',
            'unsigned' => true,
            'not null' => false,
          ),
        ),
        'rdf_property' => 'rss:description',
        'display_settings' => 
        array (
          'weight' => '1',
          'parent' => '',
          'label' => 
          array (
            'format' => 'hidden',
          ),
          'teaser' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          'full' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          4 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          2 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          3 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
        ),
      ),
      3 => 
      array (
        'label' => 'Visit course',
        'field_name' => 'field_lms_url',
        'type' => 'link',
        'widget_type' => 'link',
        'change' => 'Change basic information',
        'weight' => '-1',
        'description' => '',
        'default_value' => 
        array (
          0 => 
          array (
            'title' => '',
            'url' => '',
          ),
        ),
        'default_value_php' => '',
        'default_value_widget' => 
        array (
          'field_lms_url' => 
          array (
            0 => 
            array (
              'title' => '',
              'url' => '',
            ),
          ),
        ),
        'group' => false,
        'required' => 0,
        'multiple' => '0',
        'url' => 0,
        'title' => '',
        'title_value' => '',
        'enable_tokens' => 0,
        'display' => 
        array (
          'url_cutoff' => '80',
        ),
        'attributes' => 
        array (
          'target' => '_blank',
          'rel' => '',
          'class' => '',
        ),
        'op' => 'Save field settings',
        'module' => 'link',
        'widget_module' => 'link',
        'columns' => 
        array (
          'url' => 
          array (
            'type' => 'varchar',
            'length' => 255,
            'not null' => false,
            'sortable' => true,
          ),
          'title' => 
          array (
            'type' => 'varchar',
            'length' => 255,
            'not null' => false,
            'sortable' => true,
          ),
          'attributes' => 
          array (
            'type' => 'text',
            'size' => 'medium',
            'not null' => false,
          ),
        ),
        'rdf_property' => '',
        'display_settings' => 
        array (
          'label' => 
          array (
            'format' => 'inline',
            'exclude' => 0,
          ),
          'teaser' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          'full' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          4 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          2 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          3 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          'token' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
        ),
      ),
      4 => 
      array (
        'label' => 'Course ID',
        'field_name' => 'field_courseid',
        'type' => 'number_integer',
        'widget_type' => 'number',
        'change' => 'Change basic information',
        'weight' => 0,
        'description' => 'This field is hidden during viewing.  It is not intended to be user editable.',
        'default_value' => 
        array (
          0 => 
          array (
            'value' => '',
            '_error_element' => 'default_value_widget][field_courseid][0][value',
          ),
        ),
        'default_value_php' => '',
        'default_value_widget' => 
        array (
          'field_courseid' => 
          array (
            0 => 
            array (
              'value' => '',
              '_error_element' => 'default_value_widget][field_courseid][0][value',
            ),
          ),
        ),
        'group' => false,
        'required' => 1,
        'multiple' => '0',
        'min' => '',
        'max' => '',
        'prefix' => '',
        'suffix' => '',
        'allowed_values' => '',
        'allowed_values_php' => '',
        'op' => 'Save field settings',
        'module' => 'number',
        'widget_module' => 'number',
        'columns' => 
        array (
          'value' => 
          array (
            'type' => 'int',
            'not null' => false,
            'sortable' => true,
          ),
        ),
        'rdf_property' => 'spawar:courseid',
        'display_settings' => 
        array (
          'weight' => 0,
          'parent' => '',
          'label' => 
          array (
            'format' => 'hidden',
          ),
          'teaser' => 
          array (
            'format' => 'hidden',
            'exclude' => 0,
          ),
          'full' => 
          array (
            'format' => 'hidden',
            'exclude' => 0,
          ),
          4 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
        ),
      ),
      5 => 
      array (
        'label' => 'Is Event',
        'field_name' => 'field_is_event',
        'type' => 'number_integer',
        'widget_type' => 'optionwidgets_onoff',
        'change' => 'Change basic information',
        'weight' => '1',
        'description' => '',
        'default_value' => 
        array (
          0 => 
          array (
            'value' => NULL,
          ),
        ),
        'default_value_php' => '',
        'default_value_widget' => 
        array (
          'field_is_event' => 
          array (
            'value' => false,
          ),
        ),
        'group' => false,
        'required' => 0,
        'multiple' => '0',
        'min' => '',
        'max' => '',
        'prefix' => '',
        'suffix' => '',
        'allowed_values' => '1|Yes',
        'allowed_values_php' => '',
        'op' => 'Save field settings',
        'module' => 'number',
        'widget_module' => 'optionwidgets',
        'columns' => 
        array (
          'value' => 
          array (
            'type' => 'int',
            'not null' => false,
            'sortable' => true,
          ),
        ),
        'rdf_property' => '',
        'display_settings' => 
        array (
          'weight' => '3',
          'parent' => '',
          'label' => 
          array (
            'format' => 'hidden',
          ),
          'teaser' => 
          array (
            'format' => 'hidden',
            'exclude' => 0,
          ),
          'full' => 
          array (
            'format' => 'hidden',
            'exclude' => 0,
          ),
          4 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          2 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          3 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          'token' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
        ),
      ),
      6 => 
      array (
        'label' => 'Short name',
        'field_name' => 'field_shortname',
        'type' => 'text',
        'widget_type' => 'text_textfield',
        'change' => 'Change basic information',
        'weight' => '3',
        'rows' => 5,
        'size' => '30',
        'description' => '',
        'default_value' => 
        array (
          0 => 
          array (
            'value' => '',
            '_error_element' => 'default_value_widget][field_shortname][0][value',
          ),
        ),
        'default_value_php' => '',
        'default_value_widget' => 
        array (
          'field_shortname' => 
          array (
            0 => 
            array (
              'value' => '',
              '_error_element' => 'default_value_widget][field_shortname][0][value',
            ),
          ),
        ),
        'group' => false,
        'required' => 0,
        'multiple' => '0',
        'text_processing' => '0',
        'max_length' => '',
        'allowed_values' => '',
        'allowed_values_php' => '',
        'op' => 'Save field settings',
        'module' => 'text',
        'widget_module' => 'text',
        'columns' => 
        array (
          'value' => 
          array (
            'type' => 'text',
            'size' => 'big',
            'not null' => false,
            'sortable' => true,
          ),
        ),
        'rdf_property' => '',
        'display_settings' => 
        array (
          'label' => 
          array (
            'format' => 'inline',
            'exclude' => 0,
          ),
          'teaser' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          'full' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          4 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          2 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          3 => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
          'token' => 
          array (
            'format' => 'default',
            'exclude' => 0,
          ),
        ),
      ),
    );
    $content['extra']  = array (
      'title' => '-5',
      'menu' => '2',
    );

  return $content;
}
