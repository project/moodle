<?php

/**
 * @file
 * Admin page callbacks for the moodle module.
 */

/**
 * Implementation of hook_settings().
 */
function moodle_admin_settings() {
  $form['moodle_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Moodle URL'),
    '#default_value' => variable_get('moodle_url', ''),
    '#size' => 60,
    '#maxlength' => 244,
    '#description' => t("Specify the full URL of your Moodle installation home page.  Include the trailing slash.  For example, http://moodle.example.com/"),
  );

  $form['moodle_page_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Introductory text for Moodle course page'),
    '#default_value' => variable_get('moodle_page_intro', ''),
    '#description' => t("Any plain text or HTML desired to describe the Moodle system.  By default, this is displayed above the list of upcoming Moodle courses.  HTML can be used with a theme's CSS to adjust location if desired."),
  );

  $form['moodle_menu_link'] = array(
    '#type' => 'select',
    '#title' => t('Moodle site link menu'),
    '#default_value' => variable_get('moodle_menu_link', 'navigation'),
    '#options' => menu_get_menus(),
    '#description' => t('The maximum depth for an item and all its children   is fixed at !maxdepth. Some menu items may not be available as parents if       selecting them would exceed this limit.', array('!maxdepth' => MENU_MAX_DEPTH)),
    '#attributes' => array('class' => 'menu-title-select'),
  );

  $form['course'] = array(
    '#type' => 'fieldset',
    '#title' => t('Optional Moodle course content type'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['course']['author'] = array(
    '#type' => 'textfield',
    '#title' => t('Authored by'),
    '#size' => 50,
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => variable_get('moodle_course_author', ''),
    '#description' => t("Course content items are created automatically by !cron. This will be the username which will be used as the author of those items.  If left blank or invalid, the username 'anonymous' will be used.", array('!cron' => l('cron', 'http://drupal.org/getting-started/6/install/cron'))),
  );

  if (node_get_types('type', 'course') === FALSE && module_exists('content')) {

    // includes wrapper for contents used by ahah.js
    $form['course']['create'] = array(
      '#type' => 'submit',
      '#name' => 'create',
      '#value' => t('Create'),
      '#ahah' => array(
        'path' => 'create/js',
        'wrapper' => 'create-wrapper',
        'effect' => 'fade',
      ),
      '#submit' => array('_moodle_course_type_form_submit'),
      '#prefix' => '<div id="create-wrapper">',
      '#suffix' => '<div class="description">' . t('This content type does not yet exist.') . '</div></div>',
    );
  }
  else if (!module_exists('content')) {
    $form['course']['create'] = array(
      '#markup' => '<p>' . t('The <em>content</em> module (CCK) must be installed and enabled to use this countent type.') . '</p>',
    );
  }
  else {
    $form['course']['status'] = array(
      '#markup' => '<p>' .  t('This content type is installed.') . '</p>',
    );
  }


/*
 * Code for this feature is in pre-alpha state.
 *
  $form['moodle_cck_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Moodle integration CCK fields'),
    '#default_value' => variable_get('moodle_cck_enable', FALSE),
    '#disabled' => !module_exists('content'),
    '#description' => t('If enabled, Moodle module will generate fields for custom content types.  Requires the content module (Content Construction Kit) to be installed and enabled.') . (module_exists('content') ? '' : t('CCK is not enabled on this site.')),
  );
*/

  $form['#validate'][] = 'moodle_admin_settings_validate';
  $form = system_settings_form($form);
  $form['#submit'][] = _moodle_add_menu_link();
  return $form;
}

function moodle_admin_settings_validate($form, &$form_state) {
  if ($form_state['values']['moodle_cck_enable'] && !module_exists('content')) {
    form_set_error('', t('CCK (content module) is not installed and enabled.'));
  }
}

function _moodle_course_type_form_submit($form, &$form_state) {
  _moodle_install_course_node();
}
