<?php

// Run our cron job
_moodle_cron();

/**
 * The Moodle cron job.
 */
function _moodle_cron() {
  $courselist = _moodle_get_courses('today', '+60 days');   // todo make configable

  $coursecount = count($courselist);
  if ($coursecount > 0) {
      // initialize a list of course ids which need to be considered.
      $cids = array_combine(array_keys($courselist), array_keys($courselist) );
  }
  else {
    $cids = array();
  }

  // process each node of the moodle course type.
  $result = db_query("SELECT n.nid FROM {node} n WHERE n.type = 'course' AND n.status = 1");

  $nodecount = 0;
  // while ($node = db_fetch_object($result)) { // D6 database API
  foreach ($result as $node) {
    $nodecount++;
    $node = node_load($node->nid);    // Drupal course node
    $node_cid = (int)$node->field_courseid[0]['value']; // the node's course id

    if ($node_cid < 2) {
      watchdog('moodle', 'Node nid %nid has bogus course id %cid', array('%nid' => $node->nid, '%cid' => $node_cid), WATCHDOG_ERROR);
      continue;
    }

    if (!in_array($node_cid, $cids)) {
      // course ID no longer returned from Moodle, assume inactive/deleted.
      $node->status = 0;    // unpublish matching course node.
      node_save($node);
      watchdog('moodle', '@type: unpublished %title. Course id %cid no longer exists in Moodle.', array('@type' => $node->type, '%title' => $node->title, '%cid' => $node_cid), WATCHDOG_NOTICE, l(t('view'), 'node/' . $node->nid));
      continue;
    }

    // as we process each course id, remove it from the list
    $cids[$node_cid] = NULL;
    unset($cids[$node_cid]);

    $course = $courselist[$node->field_courseid[0]['value']];

    // maybe we should check to see if the node is more recent modified than
    // the course here?  not sure how to check. not sure it should ever occur.

    // update Drupal node
    moodle_set_node($course, _moodle_get_instructor_list($node_cid), $node);
    node_save($node);
    watchdog('moodle', '@type: updated %title.', array('@type' => $node->type, '%title' => $node->title), WATCHDOG_NOTICE, l(t('view'), 'node/' . $node->nid));
  }

  // any course ids left in $cids are those which don't have matching nodes.
  // create new course nodes for them.
  $newnodecount = 0;
  foreach ($cids as $cid) {
    unset($node);
    $node = new stdClass();
    $node->type = 'course';
    $node->field_courseid[0]['value'] = $cid;

    // we set node directly instead of using drupal_execute() due to CCK bug.
    moodle_set_node($courselist[$cid], _moodle_get_instructor_list($cid), $node);
    $node = node_submit($node);
    node_save($node);
    watchdog('moodle', '@type: added %title.', array('@type' => $node->type, '%title' => $node->title), WATCHDOG_NOTICE, l(t('view'), 'node/' . $node->nid));
    $newnodecount++;
  }
  variable_set('moodle_last_update', time());
  watchdog('moodle', "Moodle module cron processed $coursecount courses, examined $nodecount nodes and created $newnodecount nodes.");
} // end of cron job (_moodle_cron)

/**
 * Set various node structure values of course content type node in
 * preparation for saving to database.
 */
function moodle_set_node($course, $instructors, &$node) {
  // set author to admin chosen user. if not set/valid, core sets to anon.
  $node->name = variable_get('moodle_course_author', '');

  // get and convert starting date to unixtime, calculate ending date.
  // Note:  Assumes timezone of drupal and moodle are same.  todo
  list($year, $month, $day) = explode('-', $course['startdate']);
  $startdate = mktime(0, 0, 0, $month, $day, $year);
  $enddate = $startdate + ((3600*24*7) * $course['numweeks']);

  $node->title                = $course['fullname'];
  $node->changed              = $course['timemodified'];

  $node->field_shortname[0]['value'] = $course['shortname'];
  $node->field_body[0]['value']     = $course['summary'];
  $node->field_is_event[0]['value']  = '1';

  $node->field_date[0]['value']     = $startdate;
  $node->field_date[0]['value2']    = $enddate;

  $node->field_lms_url[0]['url']      = _moodle_get_url() . substr($course['urlpath'], 1);  // skip leading slash
  $node->field_lms_url[0]['title'] = check_plain($node->title);

  for ($i = 0; $i < count($instructors); $i++) {
    $node->field_name_of_instructors[$i]['value'] = $instructors[$i];
  }
}

/**
 * Get a properly formatted list of instructors for this course.
 */
function _moodle_get_instructor_list($cid) {
  $instructors = _moodle_get_instructors($cid);
  if ($instructors !== FALSE) {
    return array_values($instructors);
  }
  return array('unknown');

}
