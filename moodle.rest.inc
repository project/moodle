<?php

/**
 * @file
 * Defines the client connection to the RESTful web service provided
 * by Moodle.
 */

/**
 * Get courses for a date range from the Moodle web service.
 *
 * @param $start
 *  Starting date for courses of interest in YYYYMMDD format.
 * @param $end
 *  Ending date for courses of interest in YYYYMMDD format.
 *
 * @return -  array of course objects on success, FALSE on error
 */
function _moodle_get_courses($start = NULL, $end = NULL) {
  if (($moodle_url = _moodle_get_url()) === FALSE) return FALSE;

  if (is_null($start)) {
    $start = date('Ymd');
  }
  if (is_null($end)) {
    $end = date('Ymd', REQUEST_TIME + (86400 * 30));
  }

  $url = $moodle_url . "qapi/rest.php/course/upcoming";

  return _moodle_get_resource($url);
}


/**
 * Get courses for a user from the Moodle web service.
 *
 * @param $username
 *  The username to find courses for.
 * @param $status
 *  The status of the user in the course, e.g. inprogress, completed, enrolled.
 *
 * @return -  array of course objects on success, FALSE on error
 */
function _moodle_get_usercourses($username, $status = 'enrolled') {
  // todo: should add some username validation code here before passing it
  $username = urlencode($username);

  if (($moodle_url = _moodle_get_url()) === FALSE) return FALSE;

  $url = sprintf('%s/qapi/rest.php/course/user/%s/%s', $moodle_url, $username, $status);

  return _moodle_get_resource($url);
}

/**
 * Get the instructors for specified course id.
 *
 * @param $cid
 *  The Moodle course id to query.
 * @return - array of Moodle UID => instructor name pairs.
 */
function _moodle_get_instructors($cid) {
  if (($moodle_url = _moodle_get_url()) === FALSE) return FALSE;
  if (!is_numeric($cid)) return FALSE;
  $url = $moodle_url . "qapi/rest.php/course/instructors/$cid";
  return _moodle_get_resource($url);
}

/**
 * Get a Moodle resource.
 *
 * @param $url
 *  The RESTful resource URL to fetch.
 *
 * @return
 *  A populated array on success, or FALSE on failure.
 */
function _moodle_get_resource($url) {
  // $reply = drupal_http_request($url, $headers = array(), $method = 'GET', $data = NULL, $retry = 3);    // D6 fixme

  $reply = drupal_http_request($url, array());  // D7, defaults probably ok
  if (isset($reply->error)) {
    watchdog('moodle', t('Moodle web service returned an error.  drupal_http_request() error string was %error.'), array('%error' => $reply->error), WATCHDOG_ERROR);
    return FALSE;
  }

  $resource = json_decode($reply->data, TRUE);

  if (!is_array($resource)) {
      watchdog('moodle', t('Moodle web service returned an error.  drupal_http_request() data value was %data.'), array('%data' => $reply->data), WATCHDOG_ERROR);
      return FALSE;
  }

  if (isset($resource['error'])) {
      watchdog('moodle', t('Moodle web service returned error %msg. Requested URL  was @url.'), array('%msg' => $resource['error'], '@url' => $url), WATCHDOG_ERROR);
      return FALSE;
  }

  return $resource;
}

// --- END OF FILE ---
